jQuery(function ($) {
    $('#get-next-event-button').click(function () {
        $.ajax({
            url: '/wordpress/wp-admin/admin-ajax.php',
            type: 'POST',
            data: 'action=events',
            beforeSend: function () {
                $('#get-next-event-button').text('Loading');
            },
            success: function (data) {
                $('#get-next-event-button').text('Next events');
                console.log(JSON.parse(data))
            }
        });
    });
});

jQuery(function ($) {
    $(document).ready(function ($) {
        let citySenderSelect = $('#calculate-delivery-city-from-select'),
            cityRecipientSelect = $('#calculate-delivery-city-to-select');
        if (citySenderSelect && cityRecipientSelect) {
            $.ajax({
                url: 'https://api.novaposhta.ua/v2.0/json/',
                type: 'POST',
                contentType: 'application/json',
                crossDomain: true,
                dataType: 'jsonp',
                data: {
                    "modelName": "Address",
                    "calledMethod": "getCities",
                    "apiKey": "43b6724a68431269145da662541c1905"
                },
                success: function (data) {
                    data.data.forEach(function (item) {
                        citySenderSelect[0].appendChild(new Option(item.Description, item.Ref));
                        cityRecipientSelect[0].appendChild(new Option(item.Description, item.Ref));
                    });
                }
            });
        }
    });
});

jQuery(function ($) {
    $('#calculate-delivery-button').click(function () {

        let citySenderSelect = $('#calculate-delivery-city-from-select'),
            cityRecipientSelect = $('#calculate-delivery-city-to-select'),
            totalPrice = $('#total-price'),
            totalQuantity = $('#total-quantity');
        $.ajax({
            url: 'https://api.novaposhta.ua/v2.0/json/',
            type: 'POST',
            contentType: 'application/json',
            crossDomain: true,
            dataType: 'jsonp',
            data: {
                "modelName": "InternetDocument",
                "calledMethod": "getDocumentPrice",
                "methodProperties": {
                    "CitySender": citySenderSelect.val(),
                    "CityRecipient": cityRecipientSelect.val(),
                    "Weight": "0.5",
                    "ServiceType": "WarehouseWarehouse",
                    "Cost": totalPrice[0].value,
                    "CargoType": "Parcel",
                    "SeatsAmount": totalQuantity[0].value,
                },
                "apiKey": "43b6724a68431269145da662541c1905"
            },
            beforeSend: function () {
                $('#calculate-delivery-button').text('Loading');
            },
            success: function (data) {
                $('#calculate-delivery-button').text('Calculate delivery');
                $('#calculate-delivery-cost').text('Delivery cost: ' + data.data[0].Cost);
                console.log(data)
            }
        });
    });
});
