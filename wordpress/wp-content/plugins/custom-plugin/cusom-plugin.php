<?php
/*
Plugin Name: Custom Plugin
Description: This is custom plugin
Author: revan
Version: 1
*/
//include './config.php';
wp_enqueue_script( 'main-js', plugins_url( '/custom-plugin/main.js' ), array('jquery') );


add_action('wp_ajax_events', 'get_next_events');
add_action('wp_ajax_nopriv_events', 'get_next_events');


function get_next_events()
{
    global $wpdb;

    $result = $wpdb->get_results(
        'SELECT event_id, event_status, event_name, event_start, event_end, event_all_day, event_timezone from wp_em_events WHERE event_start > ' . time());
    wp_send_json($result, 200);
    die;
}

function next_events_button()
{
    return '<input type="button" class="button button-events" id="get-next-event-button" value="Next events">';
}

add_shortcode('next_events', 'next_events_button');


function calculate_delivery_function()
{
    global $woocommerce;
    $totalMoney= $woocommerce->cart->total ;
    $totalQuantity= $woocommerce->cart->get_cart_contents_count() ;

    $html = '</br>';
    $html .= '<select class="select2-selection__rendered" id="calculate-delivery-city-from-select">';
    $html .= '<option>';
    $html .= 'Choose your city';
    $html .= '</option>';
    $html .= '</select>';
    $html .= '</br></br>';
    $html .= '<select class="select2-selection__rendered" id="calculate-delivery-city-to-select">';
    $html .= '<option>';
    $html .= 'Choose city to send';
    $html .= '</option>';
    $html .= '</select>';
    $html .= '</br></br>';
    $html .= '<input type="hidden" id="total-price" value="'.$totalMoney.'">';
    $html .= '<input type="hidden" id="total-quantity" value="'.$totalQuantity.'">';
    $html .= '<input type="button" class="button button-delivery" id="calculate-delivery-button" value="Calculate delivery">';
    $html .= '</br></br>';
    $html .= '<div id="calculate-delivery-cost">Delivery cost:</div>';
    return $html;
}

add_shortcode('calculate_delivery', 'calculate_delivery_function');


